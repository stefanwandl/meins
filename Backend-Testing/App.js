const express = require('express');
const bodyParser = require('body-parser');
const { graphqlHTTP } = require('express-graphql');


const graphQlSchemas = require('./graphql/schemas/index');
const graphQlResolvers = require('./graphql/resolvers/index');

require('./connection');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});

app.use(
    '/graphql',
    graphqlHTTP({
        schema: graphQlSchemas,
        rootValue: graphQlResolvers,
        graphiql: true
    })
);

app.listen(3000);