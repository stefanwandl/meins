const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('session_infos', {
    sessionID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'sessions',
        key: 'sessionID'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(512),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'session_infos',
    timestamps: false,
    indexes: [
      {
        name: "fk_SESSION_INFOS_SESSIONS1_idx",
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
    ]
  });
};
