const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users_bt', {
    userID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: "username_UNIQUE"
    },
    password_hash: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    emailAddress: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: "emailAddress_UNIQUE"
    },
    stateLabel: {
      type: DataTypes.STRING(15),
      allowNull: false,
      references: {
        model: 'e_account_states',
        key: 'stateLabel'
      }
    }
  }, {
    sequelize,
    tableName: 'users_bt',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "username_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "username" },
        ]
      },
      {
        name: "userID_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "emailAddress_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "emailAddress" },
        ]
      },
      {
        name: "fk_USERS_BT_E_ACCOUNT_STATES1_idx",
        using: "BTREE",
        fields: [
          { name: "stateLabel" },
        ]
      },
    ]
  });
};
