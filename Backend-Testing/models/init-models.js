var DataTypes = require("sequelize").DataTypes;
var _admins = require("./admins");
var _e_account_states = require("./e_account_states");
var _e_module_states = require("./e_module_states");
var _e_session_states = require("./e_session_states");
var _e_user_role = require("./e_user_role");
var _module__infos = require("./module__infos");
var _modules = require("./modules");
var _session_infos = require("./session_infos");
var _sessions = require("./sessions");
var _sessions_modules_jt = require("./sessions_modules_jt");
var _user_infos = require("./user_infos");
var _users_bt = require("./users_bt");
var _users_sessions_jt = require("./users_sessions_jt");

function initModels(sequelize) {
  var admins = _admins(sequelize, DataTypes);
  var e_account_states = _e_account_states(sequelize, DataTypes);
  var e_module_states = _e_module_states(sequelize, DataTypes);
  var e_session_states = _e_session_states(sequelize, DataTypes);
  var e_user_role = _e_user_role(sequelize, DataTypes);
  var module__infos = _module__infos(sequelize, DataTypes);
  var modules = _modules(sequelize, DataTypes);
  var session_infos = _session_infos(sequelize, DataTypes);
  var sessions = _sessions(sequelize, DataTypes);
  var sessions_modules_jt = _sessions_modules_jt(sequelize, DataTypes);
  var user_infos = _user_infos(sequelize, DataTypes);
  var users_bt = _users_bt(sequelize, DataTypes);
  var users_sessions_jt = _users_sessions_jt(sequelize, DataTypes);

  modules.belongsToMany(sessions, { through: sessions_modules_jt, foreignKey: "moduleID", otherKey: "sessionID" });
  sessions.belongsToMany(modules, { through: sessions_modules_jt, foreignKey: "sessionID", otherKey: "moduleID" });
  sessions.belongsToMany(users_bt, { through: users_sessions_jt, foreignKey: "sessionID", otherKey: "userID" });
  users_bt.belongsToMany(sessions, { through: users_sessions_jt, foreignKey: "userID", otherKey: "sessionID" });
  users_bt.belongsTo(e_account_states, { as: "stateLabel_e_account_state", foreignKey: "stateLabel"});
  e_account_states.hasMany(users_bt, { as: "users_bts", foreignKey: "stateLabel"});
  modules.belongsTo(e_module_states, { as: "stateLabel_e_module_state", foreignKey: "stateLabel"});
  e_module_states.hasMany(modules, { as: "modules", foreignKey: "stateLabel"});
  sessions.belongsTo(e_session_states, { as: "stateLabel_e_session_state", foreignKey: "stateLabel"});
  e_session_states.hasMany(sessions, { as: "sessions", foreignKey: "stateLabel"});
  users_sessions_jt.belongsTo(e_user_role, { as: "roleLabel_e_user_role", foreignKey: "roleLabel"});
  e_user_role.hasMany(users_sessions_jt, { as: "users_sessions_jts", foreignKey: "roleLabel"});
  module__infos.belongsTo(modules, { as: "MODULES_module", foreignKey: "MODULES_moduleID"});
  modules.hasMany(module__infos, { as: "module__infos", foreignKey: "MODULES_moduleID"});
  sessions_modules_jt.belongsTo(modules, { as: "module", foreignKey: "moduleID"});
  modules.hasMany(sessions_modules_jt, { as: "sessions_modules_jts", foreignKey: "moduleID"});
  session_infos.belongsTo(sessions, { as: "session", foreignKey: "sessionID"});
  sessions.hasMany(session_infos, { as: "session_infos", foreignKey: "sessionID"});
  sessions_modules_jt.belongsTo(sessions, { as: "session", foreignKey: "sessionID"});
  sessions.hasMany(sessions_modules_jt, { as: "sessions_modules_jts", foreignKey: "sessionID"});
  users_sessions_jt.belongsTo(sessions, { as: "session", foreignKey: "sessionID"});
  sessions.hasMany(users_sessions_jt, { as: "users_sessions_jts", foreignKey: "sessionID"});
  admins.belongsTo(users_bt, { as: "user", foreignKey: "userID"});
  users_bt.hasOne(admins, { as: "admin", foreignKey: "userID"});
  user_infos.belongsTo(users_bt, { as: "user", foreignKey: "userID"});
  users_bt.hasMany(user_infos, { as: "user_infos", foreignKey: "userID"});
  users_sessions_jt.belongsTo(users_bt, { as: "user", foreignKey: "userID"});
  users_bt.hasMany(users_sessions_jt, { as: "users_sessions_jts", foreignKey: "userID"});

  return {
    admins,
    e_account_states,
    e_module_states,
    e_session_states,
    e_user_role,
    module__infos,
    modules,
    session_infos,
    sessions,
    sessions_modules_jt,
    user_infos,
    users_bt,
    users_sessions_jt,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
