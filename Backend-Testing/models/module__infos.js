const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('module__infos', {
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    MODULES_moduleID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'modules',
        key: 'moduleID'
      }
    }
  }, {
    sequelize,
    tableName: 'module__infos',
    timestamps: false,
    indexes: [
      {
        name: "fk_MODULE__INFOS_MODULES1_idx",
        using: "BTREE",
        fields: [
          { name: "MODULES_moduleID" },
        ]
      },
    ]
  });
};
