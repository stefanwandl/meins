const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sessions', {
    sessionID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    stateLabel: {
      type: DataTypes.STRING(15),
      allowNull: false,
      references: {
        model: 'e_session_states',
        key: 'stateLabel'
      }
    },
    code: {
      type: DataTypes.STRING(15),
      allowNull: false,
      unique: "code_UNIQUE"
    }
  }, {
    sequelize,
    tableName: 'sessions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
      {
        name: "sessionID_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
      {
        name: "code_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "fk_SESSIONS_E_SESSION_STATES1_idx",
        using: "BTREE",
        fields: [
          { name: "stateLabel" },
        ]
      },
    ]
  });
};
