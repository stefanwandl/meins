const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('E_ACTIONS', {
    action: {
      type: DataTypes.STRING(15),
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize,
    tableName: 'E_ACTIONS',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "action" },
        ]
      },
      {
        name: "action_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "action" },
        ]
      },
    ]
  });
};
