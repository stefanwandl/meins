const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('SESSIONS', {
    sessionID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    stateLabel: {
      type: DataTypes.STRING(15),
      allowNull: false,
      references: {
        model: 'E_SESSION_STATES',
        key: 'stateLabel'
      }
    },
    code: {
      type: DataTypes.STRING(15),
      allowNull: false,
      unique: "code_UNIQUE"
    },
    moduleID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'MODULES_BT',
        key: 'moduleID'
      }
    }
  }, {
    sequelize,
    tableName: 'SESSIONS',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sessionID" },
          { name: "moduleID" },
        ]
      },
      {
        name: "sessionID_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
      {
        name: "code_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "fk_SESSIONS_E_SESSION_STATES1_idx",
        using: "BTREE",
        fields: [
          { name: "stateLabel" },
        ]
      },
      {
        name: "fk_SESSIONS_MODULES_BT1_idx",
        using: "BTREE",
        fields: [
          { name: "moduleID" },
        ]
      },
    ]
  });
};
