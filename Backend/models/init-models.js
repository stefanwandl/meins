var DataTypes = require("sequelize").DataTypes;
var _ADMINS = require("./ADMINS");
var _BLACKLISTED_TOKENS = require("./BLACKLISTED_TOKENS");
var _E_ACCOUNT_STATES = require("./E_ACCOUNT_STATES");
var _E_ACTIONS = require("./E_ACTIONS");
var _E_MODULE_STATES = require("./E_MODULE_STATES");
var _E_SESSION_STATES = require("./E_SESSION_STATES");
var _E_USER_ROLE = require("./E_USER_ROLE");
var _MODULES_BT = require("./MODULES_BT");
var _MODULE_INFOS = require("./MODULE_INFOS");
var _SESSIONS = require("./SESSIONS");
var _SESSION_DATA = require("./SESSION_DATA");
var _SESSION_INFOS = require("./SESSION_INFOS");
var _USERS_BT = require("./USERS_BT");
var _USERS_SESSIONS_JT = require("./USERS_SESSIONS_JT");
var _USER_INFOS = require("./USER_INFOS");

function initModels(sequelize) {
  var ADMINS = _ADMINS(sequelize, DataTypes);
  var BLACKLISTED_TOKENS = _BLACKLISTED_TOKENS(sequelize, DataTypes);
  var E_ACCOUNT_STATES = _E_ACCOUNT_STATES(sequelize, DataTypes);
  var E_ACTIONS = _E_ACTIONS(sequelize, DataTypes);
  var E_MODULE_STATES = _E_MODULE_STATES(sequelize, DataTypes);
  var E_SESSION_STATES = _E_SESSION_STATES(sequelize, DataTypes);
  var E_USER_ROLE = _E_USER_ROLE(sequelize, DataTypes);
  var MODULES_BT = _MODULES_BT(sequelize, DataTypes);
  var MODULE_INFOS = _MODULE_INFOS(sequelize, DataTypes);
  var SESSIONS = _SESSIONS(sequelize, DataTypes);
  var SESSION_DATA = _SESSION_DATA(sequelize, DataTypes);
  var SESSION_INFOS = _SESSION_INFOS(sequelize, DataTypes);
  var USERS_BT = _USERS_BT(sequelize, DataTypes);
  var USERS_SESSIONS_JT = _USERS_SESSIONS_JT(sequelize, DataTypes);
  var USER_INFOS = _USER_INFOS(sequelize, DataTypes);

  SESSIONS.belongsToMany(USERS_BT, { through: USERS_SESSIONS_JT, foreignKey: "sessionID", otherKey: "userID" });
  SESSIONS.belongsToMany(USERS_BT, { through: USERS_SESSIONS_JT, foreignKey: "moduleID", otherKey: "userID" });
  USERS_BT.belongsToMany(SESSIONS, { through: USERS_SESSIONS_JT, foreignKey: "userID", otherKey: "sessionID" });
  USERS_BT.belongsTo(E_ACCOUNT_STATES, { as: "stateLabel_E_ACCOUNT_STATE", foreignKey: "stateLabel"});
  E_ACCOUNT_STATES.hasMany(USERS_BT, { as: "USERS_BTs", foreignKey: "stateLabel"});
  SESSION_DATA.belongsTo(E_ACTIONS, { as: "action_E_ACTION", foreignKey: "action"});
  E_ACTIONS.hasMany(SESSION_DATA, { as: "SESSION_DATa", foreignKey: "action"});
  MODULES_BT.belongsTo(E_MODULE_STATES, { as: "stateLabel_E_MODULE_STATE", foreignKey: "stateLabel"});
  E_MODULE_STATES.hasMany(MODULES_BT, { as: "MODULES_BTs", foreignKey: "stateLabel"});
  SESSIONS.belongsTo(E_SESSION_STATES, { as: "stateLabel_E_SESSION_STATE", foreignKey: "stateLabel"});
  E_SESSION_STATES.hasMany(SESSIONS, { as: "SESSIONs", foreignKey: "stateLabel"});
  USERS_SESSIONS_JT.belongsTo(E_USER_ROLE, { as: "roleLabel_E_USER_ROLE", foreignKey: "roleLabel"});
  E_USER_ROLE.hasMany(USERS_SESSIONS_JT, { as: "USERS_SESSIONS_JTs", foreignKey: "roleLabel"});
  MODULE_INFOS.belongsTo(MODULES_BT, { as: "module", foreignKey: "moduleID"});
  MODULES_BT.hasOne(MODULE_INFOS, { as: "MODULE_INFO", foreignKey: "moduleID"});
  SESSIONS.belongsTo(MODULES_BT, { as: "module", foreignKey: "moduleID"});
  MODULES_BT.hasMany(SESSIONS, { as: "SESSIONs", foreignKey: "moduleID"});
  SESSION_DATA.belongsTo(SESSIONS, { as: "session", foreignKey: "sessionID"});
  SESSIONS.hasMany(SESSION_DATA, { as: "SESSION_DATa", foreignKey: "sessionID"});
  SESSION_DATA.belongsTo(SESSIONS, { as: "module", foreignKey: "moduleID"});
  SESSIONS.hasMany(SESSION_DATA, { as: "module_SESSION_DATa", foreignKey: "moduleID"});
  SESSION_INFOS.belongsTo(SESSIONS, { as: "session", foreignKey: "sessionID"});
  SESSIONS.hasOne(SESSION_INFOS, { as: "SESSION_INFO", foreignKey: "sessionID"});
  USERS_SESSIONS_JT.belongsTo(SESSIONS, { as: "session", foreignKey: "sessionID"});
  SESSIONS.hasMany(USERS_SESSIONS_JT, { as: "USERS_SESSIONS_JTs", foreignKey: "sessionID"});
  USERS_SESSIONS_JT.belongsTo(SESSIONS, { as: "module", foreignKey: "moduleID"});
  SESSIONS.hasMany(USERS_SESSIONS_JT, { as: "module_USERS_SESSIONS_JTs", foreignKey: "moduleID"});
  ADMINS.belongsTo(USERS_BT, { as: "user", foreignKey: "userID"});
  USERS_BT.hasOne(ADMINS, { as: "ADMIN", foreignKey: "userID"});
  USERS_SESSIONS_JT.belongsTo(USERS_BT, { as: "user", foreignKey: "userID"});
  USERS_BT.hasMany(USERS_SESSIONS_JT, { as: "USERS_SESSIONS_JTs", foreignKey: "userID"});
  USER_INFOS.belongsTo(USERS_BT, { as: "user", foreignKey: "userID"});
  USERS_BT.hasOne(USER_INFOS, { as: "USER_INFO", foreignKey: "userID"});

  return {
    ADMINS,
    BLACKLISTED_TOKENS,
    E_ACCOUNT_STATES,
    E_ACTIONS,
    E_MODULE_STATES,
    E_SESSION_STATES,
    E_USER_ROLE,
    MODULES_BT,
    MODULE_INFOS,
    SESSIONS,
    SESSION_DATA,
    SESSION_INFOS,
    USERS_BT,
    USERS_SESSIONS_JT,
    USER_INFOS,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
