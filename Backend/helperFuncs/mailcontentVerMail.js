module.exports = (url, token) => {
    
    return `
  <body>
    <div style="display: flex; justify-content: center; background: white;">
        <div style="position: absolute; top: 30%; text-align: center;">
            <img src="https://cdn.discordapp.com/attachments/804262652548874269/819507982160232468/coolaborationLogo.png" alt="Logo">
            <p style="font-family: sans-serif; font-size: 1.2rem; font-weight: 400; line-height: 1.5; color: #212529; margin-top: 50px; margin-bottom: 100px"> Thanks for creating an account on our Website!</p>
            <p style="font-family: sans-serif; font-size: 1rem; font-weight: 400; line-height: 1.5; color: #212529;">Please click the button below to verify your email.</p>
            <a href="`+ url + '/' + token + `" style="display: inline-block; line-height: 1.5; color: white; font-family: sans-serif; background-color: #0d6efd; text-decoration: none; cursor: pointer; border: 1px solid transparent; padding: .375rem .75rem; font-size: 1rem; border-radius: .25rem;">Verify Email</a>
        </div>
    </div>
  </body>
    `
}