const jwt = require('jsonwebtoken');
const sequelize = require('../connection');
const DataTypes = require('sequelize').DataTypes;
const TokenBlacklist = require('../models/blacklisted_tokens')(sequelize, DataTypes);

module.exports = async (req, res, next) => {
    const authHeader = req.get('Authorization');
    if(!authHeader) {
        req.isAuth = false;
        return next();
    }
    const token = authHeader.split(' ')[1];
    if(!token){
        req.isAuth = false;
        return next();
    }
    try{
        const listedToken = await TokenBlacklist.findOne({ where: { token: token } });
        if(listedToken){
            req.isAuth = false;
            return next();
        }
    } catch(err) { throw err; }
    let decodedToken;
    try{
        decodedToken = jwt.verify(token, 'tokenkey');
    } catch (err) {
        req.isAuth = false;
        return next();
    }
    if(!decodedToken) {
        return next();
    }
    req.isAuth = true;
    req.userId = decodedToken.userId;
    req.token = token;
    next();
};