const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const Session = require('../../models/sessions')(sequelize, DataTypes);
const UserSession = require('../../models/users_sessions_jt')(sequelize, DataTypes);
const SessionInfo = require('../../models/session_infos')(sequelize, DataTypes)
const SessionData = require('../../models/session_data')(sequelize, DataTypes)

module.exports = {
    deleteSession: async (args, req) => {
        try {
            if (!req.isAuth) {
                throw new Error('Unauthenticated');
            }
            const user = await User.findOne({ where: { userID: req.userId } });

            if (!user) {
                throw new Error('User not found!');
            }

            const session = await Session.findOne({ where: { sessionID: args.sessionId } });

            if (!session) {
                throw new Error('Session not found!');
            }

            const usersession = await UserSession.findOne({ where: { sessionID: session.sessionID, userID: user.userID } });

            if (!(usersession.roleLabel == "OWNER")) {
                throw new Error('The user is not an admin!');
            }

            await SessionData.destroy({ where: { sessionID: args.sessionId } });
            await SessionInfo.destroy({ where: { sessionID: args.sessionId } });
            await UserSession.destroy({ where: { sessionID: args.sessionId } });
            await session.destroy();

            return 1;
        }
        catch (err) {
            throw err;
        }
    }
};