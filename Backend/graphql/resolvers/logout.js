const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const TokenBlacklist = require('../../models/blacklisted_tokens')(sequelize, DataTypes);

module.exports = {
    logout: async (args, req) => {
        if (!req.isAuth) {
            throw new Error('Unauthenticated');
        }
        const user = await User.findOne({ where: { userID: req.userId } });

        if (!user) {
            throw new Error('User not found!');
        }
        try {
            const listEntery = await TokenBlacklist.build({
                token: req.token,
                tokenDate: new Date()
            });
            const result = await listEntery.save();
            return 200;
        }
        catch (err) { console.log(err) }
    }
};