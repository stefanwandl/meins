const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);

module.exports = {
    testToken: async (args, req) => {
        if (!req.isAuth) {
            throw new Error('Unauthenticated');
        }
        const user = await User.findOne({ where: { userID: req.userId } });

        if (!user) {
            throw new Error('User not found!');
        }
        if(user){
            return user.username;
        }
        return "false";
    }
};