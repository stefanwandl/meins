const jwt = require('jsonwebtoken');
const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const sendMail = require('../../helperFuncs/sendMail');
const verContent = require('../../helperFuncs/mailcontentVerMail');

module.exports = {
    createUser: async args => {
        try {
            let existingUser = await User.findOne({ where: { emailAddress: args.userInput.email } });
            if (existingUser)
                throw new Error("Email already registered!");
            existingUser = await User.findOne({ where: { username: args.userInput.username } });
            if (existingUser)
                throw new Error("Username taken!");

            const user = await User.build({
                username: args.userInput.username,
                emailAddress: args.userInput.email,
                stateLabel: "NORMAL"
            });
            const result = await user.save();

            //console.log(result);

            const token = jwt.sign({ userId: result.userID, userState: result.stateLabel }, 'tokenkey', {
                expiresIn: '1h'
            });
            sendMail(verContent('http://coolaboration.tk/verify-email', token), 'Please verify E-Mail-address', 'Email Verfication | Coolaboration', result.emailAddress);
            return { userId: result.userID, token: token, expiration: 1 };
        }
        catch (err) { throw err }
    },
    getUsers: () => {
        return "test";
    }
}