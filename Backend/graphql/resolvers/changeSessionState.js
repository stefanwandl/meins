const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const Session = require('../../models/sessions')(sequelize, DataTypes);
const UserSession = require('../../models/users_sessions_jt')(sequelize, DataTypes);

module.exports = {
    changeSessionState: async(args, req) => {
        
        if(!req.isAuth){
            throw new Error('Unauthenticated');
        }
        const user = await User.findOne({ where: { userID: req.userId } });

        if(!user){
            throw new Error('User not found!');
        }

        const session = await Session.findOne({ where: { code: args.sessionState.code } });

        if(!session){
            throw new Error('Session not found!');
        }

        const usersession = await  UserSession.findOne({ where: { sessionID: session.sessionID, userID: user.userID} });

        if(!(usersession.roleLabel == "OWNER")){
            throw new Error('The user is not an admin!');
        }

        if(args.sessionState.state == "FROZEN"){
            session.stateLabel = "FROZEN";
            await session.save();
        }

        else if(args.sessionState.state == "ACTIVE"){
            session.stateLabel = "ACTIVE";
            await session.save();
        }

        else{
            throw new Error('The wanted state is not applicable!');
        }

        return "The state changed to: " + session.stateLabel;
    }
};