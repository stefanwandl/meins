module.exports = (
    `
        type PwResetReturn {
            userId: ID!
            expiration: Int!
            token: String!
        }
    `
);