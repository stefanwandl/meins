module.exports = (`
    type AuthData {
        userId: ID!
        token: String!
    }
`);