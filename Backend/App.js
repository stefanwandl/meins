const express = require('express');
const bodyParser = require('body-parser');
const { graphqlHTTP } = require('express-graphql');


const graphQlSchemas = require('./graphql/schemas/index');
const graphQlResolvers = require('./graphql/resolvers/index');

const isAuth = require('./middleware/is-auth');

require('./connection');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});

app.use(isAuth);

app.use(
    '/graphql',
    graphqlHTTP({
        schema: graphQlSchemas,
        rootValue: graphQlResolvers,
        graphiql: true
    })
);

app.listen(4000);

// --- CHAT ---
const app_chat = require("express")();
const http_chat = require("http").createServer(app_chat);
const io = require("socket.io")(http_chat, {
  cors: {
    origin: "*",
  },
});

io.on("connection", (socket) => {

  console.log("connected");
  
  socket.on("message", ({ name, message, timestamp }) => {
    io.emit("message", { name, message, timestamp });
  });

  socket.on("disconnect", () => {
    console.log("disconnected");
  });
});


http_chat.listen(4001, function () {
  console.log("listening on port 4001");
});