-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema coolaboration
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `coolaboration` ;

-- -----------------------------------------------------
-- Schema coolaboration
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `coolaboration` DEFAULT CHARACTER SET utf8 ;
USE `coolaboration` ;

-- -----------------------------------------------------
-- Table `coolaboration`.`E_ACCOUNT_STATES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`E_ACCOUNT_STATES` (
  `stateLabel` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`stateLabel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`USERS_BT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`USERS_BT` (
  `userID` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password_hash` VARCHAR(255) NULL,
  `emailAddress` VARCHAR(255) NOT NULL,
  `stateLabel` VARCHAR(15) NOT NULL,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  PRIMARY KEY (`userID`),
  UNIQUE INDEX `userID_UNIQUE` (`userID` ASC),
  UNIQUE INDEX `emailAddress_UNIQUE` (`emailAddress` ASC),
  INDEX `fk_USERS_BT_E_ACCOUNT_STATES1_idx` (`stateLabel` ASC),
  CONSTRAINT `fk_USERS_BT_E_ACCOUNT_STATES1`
    FOREIGN KEY (`stateLabel`)
    REFERENCES `coolaboration`.`E_ACCOUNT_STATES` (`stateLabel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `coolaboration`.`USER_INFOS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`USER_INFOS` (
  `firstName` VARCHAR(50) NOT NULL,
  `lastName` VARCHAR(50) NOT NULL,
  `userID` INT NOT NULL,
  INDEX `fk_USER_INFOS_USERS_BT1_idx` (`userID` ASC),
  PRIMARY KEY (`userID`),
  CONSTRAINT `fk_USER_INFOS_USERS_BT1`
    FOREIGN KEY (`userID`)
    REFERENCES `coolaboration`.`USERS_BT` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `coolaboration`.`ADMINS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`ADMINS` (
  `userID` INT NOT NULL,
  PRIMARY KEY (`userID`),
  CONSTRAINT `fk_ADMINS_USERS_BT1`
    FOREIGN KEY (`userID`)
    REFERENCES `coolaboration`.`USERS_BT` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`E_SESSION_STATES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`E_SESSION_STATES` (
  `stateLabel` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`stateLabel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`E_MODULE_STATES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`E_MODULE_STATES` (
  `stateLabel` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`stateLabel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`MODULES_BT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`MODULES_BT` (
  `moduleID` INT NOT NULL AUTO_INCREMENT,
  `stateLabel` VARCHAR(15) NOT NULL,
  `code` VARCHAR(16384) NOT NULL,
  PRIMARY KEY (`moduleID`),
  UNIQUE INDEX `moduleID_UNIQUE` (`moduleID` ASC),
  INDEX `fk_MODULES_E_MODULE_STATES1_idx` (`stateLabel` ASC),
  CONSTRAINT `fk_MODULES_E_MODULE_STATES1`
    FOREIGN KEY (`stateLabel`)
    REFERENCES `coolaboration`.`E_MODULE_STATES` (`stateLabel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`SESSIONS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`SESSIONS` (
  `sessionID` INT NOT NULL AUTO_INCREMENT,
  `stateLabel` VARCHAR(15) NOT NULL,
  `code` VARCHAR(15) NOT NULL,
  `moduleID` INT NOT NULL,
  PRIMARY KEY (`sessionID`, `moduleID`),
  UNIQUE INDEX `sessionID_UNIQUE` (`sessionID` ASC),
  INDEX `fk_SESSIONS_E_SESSION_STATES1_idx` (`stateLabel` ASC),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  INDEX `fk_SESSIONS_MODULES_BT1_idx` (`moduleID` ASC),
  CONSTRAINT `fk_SESSIONS_E_SESSION_STATES1`
    FOREIGN KEY (`stateLabel`)
    REFERENCES `coolaboration`.`E_SESSION_STATES` (`stateLabel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SESSIONS_MODULES_BT1`
    FOREIGN KEY (`moduleID`)
    REFERENCES `coolaboration`.`MODULES_BT` (`moduleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`MODULE_INFOS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`MODULE_INFOS` (
  `name` VARCHAR(50) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `moduleID` INT NOT NULL,
  INDEX `fk_MODULE__INFOS_MODULES1_idx` (`moduleID` ASC),
  PRIMARY KEY (`moduleID`),
  CONSTRAINT `fk_MODULE__INFOS_MODULES1`
    FOREIGN KEY (`moduleID`)
    REFERENCES `coolaboration`.`MODULES_BT` (`moduleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`SESSION_INFOS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`SESSION_INFOS` (
  `sessionID` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(512) NOT NULL,
  INDEX `fk_SESSION_INFOS_SESSIONS1_idx` (`sessionID` ASC),
  PRIMARY KEY (`sessionID`),
  CONSTRAINT `fk_SESSION_INFOS_SESSIONS1`
    FOREIGN KEY (`sessionID`)
    REFERENCES `coolaboration`.`SESSIONS` (`sessionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`E_USER_ROLE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`E_USER_ROLE` (
  `roleLabel` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`roleLabel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`USERS_SESSIONS_JT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`USERS_SESSIONS_JT` (
  `userID` INT NOT NULL,
  `roleLabel` VARCHAR(15) NOT NULL,
  `sessionID` INT NOT NULL,
  `moduleID` INT NOT NULL,
  PRIMARY KEY (`userID`, `sessionID`, `moduleID`),
  INDEX `fk_USERS_BT_has_SESSIONS_USERS_BT1_idx` (`userID` ASC),
  INDEX `fk_USERS_SESSIONS_JT_E_USER_ROLE1_idx` (`roleLabel` ASC),
  INDEX `fk_USERS_SESSIONS_JT_SESSIONS1_idx` (`sessionID` ASC, `moduleID` ASC),
  CONSTRAINT `fk_USERS_BT_has_SESSIONS_USERS_BT1`
    FOREIGN KEY (`userID`)
    REFERENCES `coolaboration`.`USERS_BT` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_USERS_SESSIONS_JT_E_USER_ROLE1`
    FOREIGN KEY (`roleLabel`)
    REFERENCES `coolaboration`.`E_USER_ROLE` (`roleLabel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_USERS_SESSIONS_JT_SESSIONS1`
    FOREIGN KEY (`sessionID` , `moduleID`)
    REFERENCES `coolaboration`.`SESSIONS` (`sessionID` , `moduleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `coolaboration`.`BLACKLISTED_TOKENS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`BLACKLISTED_TOKENS` (
  `tokenId` INT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(255) NOT NULL,
  `tokenDate` DATE NOT NULL,
  PRIMARY KEY (`tokenId`),
  UNIQUE INDEX `tokenId_UNIQUE` (`tokenId` ASC),
  UNIQUE INDEX `token_UNIQUE` (`token` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`E_ACTIONS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`E_ACTIONS` (
  `action` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`action`),
  UNIQUE INDEX `action_UNIQUE` (`action` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coolaboration`.`SESSION_DATA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coolaboration`.`SESSION_DATA` (
  `sessionDataID` INT NOT NULL AUTO_INCREMENT,
  `data` VARCHAR(512) NOT NULL,
  `sessionID` INT NOT NULL,
  `moduleID` INT NOT NULL,
  `timestamp` DATETIME(6) NOT NULL,
  `action` VARCHAR(15) NOT NULL,
  `sessionUsername` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`sessionDataID`),
  INDEX `fk_SESSION_DATA_SESSIONS1_idx` (`sessionID` ASC, `moduleID` ASC),
  INDEX `fk_SESSION_DATA_E_ACTIONS1_idx` (`action` ASC),
  UNIQUE INDEX `timestamp_UNIQUE` (`timestamp` ASC),
  CONSTRAINT `fk_SESSION_DATA_SESSIONS1`
    FOREIGN KEY (`sessionID` , `moduleID`)
    REFERENCES `coolaboration`.`SESSIONS` (`sessionID` , `moduleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SESSION_DATA_E_ACTIONS1`
    FOREIGN KEY (`action`)
    REFERENCES `coolaboration`.`E_ACTIONS` (`action`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO E_ACCOUNT_STATES (stateLabel) VALUES ('BANNED');
INSERT INTO E_ACCOUNT_STATES (stateLabel) VALUES ('UNBANNED');
INSERT INTO E_ACCOUNT_STATES (stateLabel) VALUES ('NORMAL');

INSERT INTO USERS_BT (username, password_hash, emailAddress, stateLabel) VALUES ('max', '$2a$12$ZSwtbPIy/L4XDjH0QiMsFuio32rgZrKxqjIG9Q4CU7pVECYnfn1By', 'max@mustermann.boahn', 'BANNED');
INSERT INTO USERS_BT (username, password_hash, emailAddress, stateLabel) VALUES ('fandl', '$2a$12$ZSwtbPIy/L4XDjH0QiMsFuio32rgZrKxqjIG9Q4CU7pVECYnfn1By', 'f.fandl@htlkrems.at', 'UNBANNED');
INSERT INTO USERS_BT (username, password_hash, emailAddress, stateLabel) VALUES ('scherzer', '$2a$12$ZSwtbPIy/L4XDjH0QiMsFuio32rgZrKxqjIG9Q4CU7pVECYnfn1By', 't.scherzer@htlkrems.at', 'NORMAL');

INSERT INTO ADMINS (userID) VALUES (2);
INSERT INTO ADMINS (userID) VALUES (3);

INSERT INTO USER_INFOS (firstName, lastName, userID) VALUES ('Fabian', 'Fandl', 2);
INSERT INTO USER_INFOS (firstName, lastName, userID) VALUES ('Tobias', 'Scherzer', 3);

INSERT INTO E_SESSION_STATES (stateLabel) VALUES ('FROZEN');
INSERT INTO E_SESSION_STATES (stateLabel) VALUES ('ACTIVE');

INSERT INTO E_MODULE_STATES (stateLabel) VALUES ('RELEASED');
INSERT INTO E_MODULE_STATES (stateLabel) VALUES ('WIP'); -- Work In Progress (needs config)

INSERT INTO MODULES_BT (stateLabel, code) VALUES ('RELEASED', 'printf("fprint");');
INSERT INTO MODULES_BT (stateLabel, code) VALUES ('WIP', 'printf("moin");"');

INSERT INTO SESSIONS (stateLabel, code, moduleID) VALUES ('ACTIVE', 'HAHAHAHAHAHAHAH', 1);
INSERT INTO SESSIONS (stateLabel, code, moduleID) VALUES ('FROZEN', 'BRRRRRRRRRRRRRR', 1);
INSERT INTO SESSIONS (stateLabel, code, moduleID) VALUES ('ACTIVE', 'NOOOOOOOOOOOOOO', 2);

INSERT INTO E_USER_ROLE (roleLabel) VALUES ('OWNER');
INSERT INTO E_USER_ROLE (roleLabel) VALUES ('GUEST');
INSERT INTO E_USER_ROLE (roleLabel) VALUES ('VIEWER');
INSERT INTO E_USER_ROLE (roleLabel) VALUES ('PARTICIPANT');

INSERT INTO USERS_SESSIONS_JT (userID, roleLabel, sessionID, moduleID) VALUES (2, 'GUEST', 1, 1);
INSERT INTO USERS_SESSIONS_JT (userID, roleLabel, sessionID, moduleID) VALUES (3, 'OWNER', 1, 1);

INSERT INTO SESSION_INFOS (sessionID, name, description) VALUES (1, 'Number 1', 'This is a cool session');
INSERT INTO SESSION_INFOS (sessionID, name, description) VALUES (2, 'Dead Session', 'Just like Yopi');

INSERT INTO MODULE_INFOS (name, description, moduleID) VALUES ('Drawing Tool', 'Tool 4 cool draws', 1);
INSERT INTO MODULE_INFOS (name, description, moduleID) VALUES ('AI Drawing', 'Brunner confirmed AI', 2);

INSERT INTO E_ACTIONS (action) VALUES ('TRANSMIT');
INSERT INTO E_ACTIONS (action) VALUES ('ALTER');
INSERT INTO E_ACTIONS (action) VALUES ('DELETE');

INSERT INTO SESSION_DATA (data, sessionID, moduleID, timestamp, action, sessionUsername) VALUES ('hallo', 1, 1, CURRENT_TIMESTAMP(6), 'TRANSMIT', 'scherzer');
INSERT INTO SESSION_DATA (data, sessionID, moduleID, timestamp, action, sessionUsername) VALUES ('goschn', 3, 2, CURRENT_TIMESTAMP(6), 'ALTER', 'fandl');

commit;
