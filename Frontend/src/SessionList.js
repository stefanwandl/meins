import React, { useRef, useState, useEffect } from 'react'
import {
    useHistory
} from "react-router-dom";
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Overlay from 'react-bootstrap/Overlay';
import Tooltip from 'react-bootstrap/Tooltip';
import Spinner from './components/Spinner';
import AddSessionModal from './components/AddSessionModal';

function SessionList() {
    const history = useHistory();

    const shareRef = useRef(null);
    const shareButtonRef = useRef(null);
    const [copyTooltip, setCopyTooltip] = useState(false);

    const [sessions, setSessions] = useState(null);
    const [showAddSessionModal, setShowAddSessionModal] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            await fetch('http://207.246.94.140:4000/graphql', {
                method: 'POST',
                body: JSON.stringify(
                    {
                        query: `
                            query {
                                getAllSessions {
                                    sessionId,
                                    name,
                                    desc,
                                    sessionCode,
                                    sessionState,
                                    currentRole,
                                    moduleName,
                                    moduleId
                                }
                            }
                    `}
                ),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + window.localStorage.getItem("token")
                }
            }).then(res => res.json()).then(resData => {
                setSessions(resData.data.getAllSessions);
            }).catch(err => console.log(err));
        }

        fetchData();

    }, [])

    async function handleCreate(newSession) {
        await fetch('http://207.246.94.140:4000/graphql', {
            method: 'POST',
            body: JSON.stringify({
                query: `
                    mutation {
                        createSession(sessionInput: {name: "${newSession.name}", desc: "", moduleId: ${Number(newSession.type)}}) {
                            sessionId,
                            sessionCode,
                            moduleName
                        }
                    } `
            }),
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + window.localStorage.getItem("token") }
        })
            .then(res => res.json())
            .then(res => {
                setSessions((sessions) => [...sessions, { sessionId: res.data.createSession.sessionId, sessionState: "ACTIVE", sessionCode: res.data.createSession.sessionCode, moduleName: res.data.createSession.moduleName, currentRole: "OWNER", name: newSession.name, moduleId: newSession.type, }])
            }).catch(error => console.log(error));
    }


    function copyToClipboard() {
        shareRef.current.select();
        document.execCommand('copy');
        setCopyTooltip(true);
        setTimeout(() => setCopyTooltip(false), 2000);
    }


    async function leaveSession(session) {
        await fetch('http://207.246.94.140:4000/graphql', {
            method: 'POST',
            body: JSON.stringify({
                query: `
                        mutation {
                            leaveSession(sessionId: ${Number(session.sessionId)})
                        } `
            }
            ),
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + window.localStorage.getItem("token") }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
                throw new Error('Network error!');
            }
            return res.json();
        }).then(resData => {
            let newSessions = sessions.slice();
            for (let i = 0; i < newSessions.length; i++) {
                if (newSessions[i].sessionId == session.sessionId) {
                    newSessions.splice(i, 1);
                }
            }
            setSessions(newSessions);
        }).catch(err => {
            console.log(err);
        });
    }

    async function changeState(session, state) {
        await fetch('http://207.246.94.140:4000/graphql', {
            method: 'POST',
            body: JSON.stringify({
                query: `
                        mutation {
                            changeSessionState(sessionState: {state: "${state}", code: "${session.sessionCode}"})
                        } `
            }),
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + window.localStorage.getItem("token") }
        }).then(res => res.json()
        ).then(res => {
            let newSessions = sessions.slice();
            for (let i = 0; i < newSessions.length; i++) {
                if (newSessions[i].sessionId == session.sessionId) {
                    newSessions[i].sessionState = state;
                }
            }
            setSessions(newSessions);
        }).catch(error => console.log(error));
    }

    function openSession(session) {
        history.push("/session/" + session.moduleId + "/" + session.sessionCode);
    }

    async function deleteSession(session) {
        await fetch('http://207.246.94.140:4000/graphql', {
            method: 'POST',
            body: JSON.stringify({
                query: `
                        mutation {
                            deleteSession(sessionId: ${Number(session.sessionId)})
                        } `
            }
            ),
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + window.localStorage.getItem("token") }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
                throw new Error('Network error!');
            }
            return res.json();
        }).then(resData => {
            let newSessions = sessions.slice();
            for (let i = 0; i < newSessions.length; i++) {
                if (newSessions[i].sessionId == session.sessionId) {
                    newSessions.splice(i, 1);
                }
            }
            setSessions(newSessions);
        }).catch(err => {
            console.log(err);
        });
    }

    return (
        <div className="container custom-form-1000 p-5">

            <div className="card">
                <Button variant="success" className="btn btn-primary w-100" onClick={() => setShowAddSessionModal(true)}>
                    <i className="fas fa-plus-circle"></i>
                </Button>
            </div>

            <AddSessionModal show={showAddSessionModal} handleCreate={(newSession) => handleCreate(newSession)} handleClose={() => setShowAddSessionModal(false)} />

            { sessions !== null ?
                sessions.length > 0 ?
                    sessions.map((session) => {
                        return (
                            <div className={session.sessionState == "FROZEN" ? "disabled card mt-2" : "card mt-2"}>
                                <h4 className="card-header d-flex justify-content-between">
                                    <span>{session.name}</span>
                                    <Button variant="primary" className="btn btn-primary" onClick={() => openSession(session)}>
                                        <i className="fas fa-play"></i>
                                    </Button>
                                </h4>

                                <div className="card-body d-flex justify-content-between">
                                    <div>
                                        <h5 className="m-3">{session.moduleName}</h5>
                                    </div>
                                    <div>

                                        {
                                            session.currentRole == "OWNER" && (
                                                session.sessionState == "ACTIVE" ?
                                                    <button className="btn btn-primary-outline" type="button" onClick={() => changeState(session, "FROZEN")}>
                                                        <i className="fas fa-snowflake m-3"></i>
                                                    </button>
                                                    :
                                                    <button className="btn btn-primary-outline" type="button" onClick={() => changeState(session, "ACTIVE")}>
                                                        <i className="fas fa-fire m-3"></i>
                                                    </button>
                                            )
                                        }


                                        <button className="btn btn-primary-outline" type="button" data-bs-toggle="collapse" data-bs-target={"#ShareCollapse" + session.sessionId} aria-expanded="false">
                                            <i className="fas fa-share-square m-3"></i>
                                        </button>

                                        {
                                            session.currentRole == "OWNER" &&
                                            <button className="btn btn-primary-outline" type="button" onClick={() => deleteSession(session)}>
                                                <i className="fas fa-trash m-3"></i>
                                            </button>
                                        }

                                        {session.currentRole != "OWNER" &&
                                            <button className="btn btn-primary-outline" type="button" onClick={() => leaveSession(session)}>
                                                <i className="fas fa-sign-out-alt m-3"></i>
                                            </button>
                                        }
                                    </div>
                                </div>

                                <div className="collapse multi-collapse" id={"ShareCollapse" + session.sessionId}>
                                    <div className="card card-body">
                                        <div className="input-group mb-3">
                                            <input class="form-control" ref={shareRef} type="text" readOnly value={"https://coolaboration.tk/join/" + session.sessionCode} />
                                            <button ref={shareButtonRef} className="btn btn-primary" type="button" onClick={copyToClipboard}>
                                                <i className="fas fa-clipboard m-1"></i>
                                            </button>
                                            <Overlay target={shareButtonRef.current} show={copyTooltip} placement="top">
                                                {(props) => (
                                                    <Tooltip id="overlay-example" {...props}>
                                                        Copied!
                                                    </Tooltip>
                                                )}
                                            </Overlay>
                                        </div>
                                    </div>
                                </div>

                                <div className="collapse multi-collapse" id={"UsersCollapse" + session.sessionId}>
                                    <div className="card card-body">
                                        <ul className="list-group">
                                            <li className="list-group-item"><i className="fas mx-2 fa-crown"></i>Admin</li>
                                            <li className="list-group-item"><i className="fas mx-4 fa-user"></i>User 1</li>
                                            <li className="list-group-item"><i className="fas mx-4 fa-user"></i>User 2</li>
                                            <li className="list-group-item"><i className="fas mx-4 fa-user"></i>User 3</li>
                                            <li className="list-group-item"><i className="fas mx-4 fa-user"></i>User 4</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                    :
                    <div className="alert alert-secondary mt-3" role="alert">
                        No session yet. Either create or join one.
                    </div>
                :
                <Spinner />
            }
        </div>
    )
}

export default SessionList
