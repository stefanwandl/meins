import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { React, useState, useMemo, useEffect } from 'react';
import './App.css';
import SignIn from './SignIn.js';
import Dashboard from './Dashboard.js';
import ForgotPassword from './ForgotPassword.js';
import SignUp from './SignUp.js';
import Confirm from "./Confirm";
import { UserContext } from "./UserContext";
import Visitor from "./Visitor";
import Session from "./Session";
import Spinner from "./components/Spinner.js";
import { NameContext } from "./components/NameContext";



function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const providerValue = useMemo(() => ({ isLoggedIn, setIsLoggedIn }), [isLoggedIn, setIsLoggedIn]); // keine Ahnung warum ma des braucht owa irgendwie was ma don überoi ob ma ogmödt is oda nd
  const [justSetThePassword, setJustSetThePassword] = useState(false);
  const [name, setUsername] = useState("");

  useEffect(() => {

    fetch('http://207.246.94.140:4000/graphql', {
      method: 'POST',
      body: JSON.stringify(
        {
          query: `
                  query {
                      testToken
                  }
          `}
      ),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      }
    }).then(res => res.json()).then(resData => {
      if (resData.errors === undefined) {
        setUsername(resData.data.testToken);
        setIsLoggedIn(true);
        console.log("logged in");
      } else {
        setIsLoggedIn(false);
        console.log("not logged in");
      }
      setIsLoading(false);
    }).catch(err => {
      console.log(err);
    });
  }, []);

  //spinner animation while page is loading
  if (isLoading) {
    return <Spinner />
  } else {
    return (
      <UserContext.Provider value={providerValue}>
        <NameContext.Provider value={{name, setUsername}}>
          <Router>
            <Switch>
              <Route path="/signin">
                <SignIn justSetThePassword={justSetThePassword} setJustSetThePassword={setJustSetThePassword} />
              </Route>
              <Route path="/signup">
                <SignUp />
              </Route>
              <Route path="/join/:sessionCode">
                <Visitor />
              </Route>
              <Route path="/join">
                <Visitor />
              </Route>
              <Route path="/forgot-password">
                <ForgotPassword />
              </Route>
              <Route path="/verify-email/:token">
                <Confirm emailVerfication={true} setJustSetThePassword={setJustSetThePassword}></Confirm>
              </Route>
              <Route path="/change-password/:token">
                <Confirm emailVerfication={false} setJustSetThePassword={setJustSetThePassword}></Confirm>
              </Route>
              <Route path="/session/:moduleId/:gameCode" exact>
                <Session />
              </Route>
              <Route path="/dashboard">
                {
                  isLoggedIn ? <Dashboard /> : <Redirect to="/signin" />
                }
              </Route>
              <Route
                path="/"
                render={() => {
                  return isLoggedIn ? <Redirect to="/dashboard" /> : <Redirect to="/join" />
                }}
              />
            </Switch>

          </Router>
        </NameContext.Provider>
      </UserContext.Provider>
    );
  }
}

export default App;