import React from 'react'
import { Link } from 'react-router-dom'

export default function SessionHeader() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid d-flex justify-content-between">
                <Link className="navbar-brand me-auto" to="/dashboard"><img src="https://cdn.discordapp.com/attachments/804262652548874269/819507982160232468/coolaborationLogo.png" alt="Logo" width="auto" height="24"/></Link>
            </div>
        </nav>
    )
}
