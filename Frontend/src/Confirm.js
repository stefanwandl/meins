import React, { useState, useEffect } from 'react';
import {
    useParams,
    useHistory 
  } from "react-router-dom";

function Confirm({ emailVerfication, setJustSetThePassword }) {
    let { token } = useParams();
    let history = useHistory();

    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [passwordError, setPasswordError] = useState("");
    const [confirmationPasswordError, setConfirmationPasswordError] = useState("");

    //token is the right one
    useEffect(() => {
        if (localStorage.getItem('token') !== token) {
            history.push("/signin");
        }
    }, [history, token]);

    //password criteria check (laufend)
    function passwordCriteriaCheck() {
        let passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,128}$/;
        if(!password.match(passw)) { 
            setPasswordError("Password criteria not met");
        } else {
            setPasswordError("");
        }
    }

    function handlePasswordChosen() {
        setConfirmationPasswordError("");
        
        let _confirmationPasswordError = "";

        if (password !== confirmPassword){
            _confirmationPasswordError = "The passwords don't match";
        } 
        setConfirmationPasswordError(_confirmationPasswordError);

        if (passwordError === "" && _confirmationPasswordError === "") {
            // Pw in datenbank speichern
            fetch('http://207.246.94.140:4000/graphql', {
                method: 'POST',
                body: JSON.stringify(
                    {
                        query: `
                            mutation {
                                changePassword(password: "${password}") {
                                    userId,
                                    token
                                }
                            }
                    `}
                ),
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': 'Bearer ' + window.localStorage.getItem("token")
                }
              }).then(res => {
                if (res.status !== 200 && res.status !== 201 && res.status !== 500){
                  throw new Error('Network error!');
                }
                return res.json();
              }).then(resData => {
                //localStorage.setItem('token', resData.data.createUser.token);
                    history.push("/signin");
                    setJustSetThePassword(true);
              }).catch(err => {
                console.log(err);
              });
            
        }
    }

    return (
        <div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
            <form className="Login container custom-form" onSubmit={(e) => e.preventDefault()}>
                {
                    emailVerfication && 
                    <div className="alert alert-success" role="alert">
                        Your email has been verified! Now you need to choose a password
                    </div>
                }
                <div className="mb-3">
                    <label htmlFor="password" className="form-label" aria-describedby="passwordHelp">Password</label>
                    <input type="password" className={passwordError !== "" ? "form-control is-invalid" : "form-control"} id="password"  onChange={(e) => setPassword(e.target.value)} onBlur={passwordCriteriaCheck} value={password} />
                    <div className={passwordError !== ""  ? "invalid-feedback" : "hidden"} >
                        {passwordError}
                    </div>
                    <div id="passwordHelp" className="form-text">
                        <span>Your password must have at least</span>
                        <ul>
                            <li>8 characters</li>
                            <li>1 number</li>
                            <li>1 upper-case letter</li>
                            <li>1 lower-case letter</li>
                        </ul>
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="confirmPassword" className="form-label" aria-describedby="confirmPasswordHelp">Confirm password</label>
                    <input type="password" className={confirmationPasswordError !== "" ? "form-control is-invalid" : "form-control"} id="confirmPassword"  onChange={(e) => setConfirmPassword(e.target.value)}  value={confirmPassword} />
                    <div className={confirmationPasswordError !== ""  ? "invalid-feedback" : "hidden"} >
                        {confirmationPasswordError}
                    </div>
                    <div id="confirmPasswordHelp" className="form-text">To make sure there aren't any typos</div>
                </div>
                <button type="submit" className="btn btn-primary btn-block w-100"  onClick={handlePasswordChosen}>Choose password</button>
            </form>

        </div>
    );
}

export default Confirm;