
import React, { useState, useRef } from 'react';
import { Link, Redirect } from 'react-router-dom';
 
function Settings() {
    const [gotEmail, setGotEmail] = useState(false);
    const passwordRef = useRef(null);

    function handleChangePassword() {

        fetch('http://207.246.94.140:4000/graphql', {
            method: 'POST',
            body: JSON.stringify(
                {
                    query: `
                            query {
                                passwdChangeMail {
                                    userId,
                                    token
                                }
                            }
                    `}
            ),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + window.localStorage.getItem("token")
            }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
                throw new Error('Network error!');
            }
            return res.json();
        }).then(resData => {
            setGotEmail(true);
            localStorage.setItem('token', resData.data.passwdChangeMail.token)
        }).catch(err => {
            console.log(err);
        });

    }

    async function handleDeleteAccount(e) {
        e.preventDefault()

        await fetch('http://207.246.94.140:4000/graphql', {
            method: 'POST',
            body: JSON.stringify(
                {
                    query: `
                        mutation {
                            deleteUser(password: "${passwordRef.current.value}")
                    }
            `}
            ),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + window.localStorage.getItem("token")
            }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
                throw new Error('Network error!');
            }
            return res.json();
        }).then(resData => {
            console.log(resData);
        }).catch(err => {
            console.log(err);
        });
        <Redirect to="/signin" />
    }

    return (
        <div className="Settings container p-5 custom-form">
            {
                gotEmail &&
                <div className="alert alert-success" role="alert">
                    An email with instructions to reset your password was sent to you. Please click on the link in it.
                </div>
            }

            <div className="d-flex justify-content-between">
                <button type="submit" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#changePasswordModal">Change Password</button>
                <button type="button" className="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteAccountModal">Delete Account</button>
            </div>

            <div className="modal fade" id="changePasswordModal" tabIndex="-1" aria-labelledby="changePasswordModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="deleteAccountModalLabel">Do you really want to change your password?</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <p>
                                    You'll get an email with instructions to reset your password and you'll be logged out.
                            </p>
                                <div className="d-flex justify-content-between">
                                    <button type="button" className="btn btn-secondary mr-5" data-bs-dismiss="modal">Cancel</button>
                                    <button data-bs-dismiss="modal" className="btn btn-primary" onClick={handleChangePassword}>Change Password</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="deleteAccountModal" tabIndex="-1" aria-labelledby="deleteAccountModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="deleteAccountModalLabel">Delete your Account</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <div className="mb-3">
                                    <label htmlFor="Password" className="form-label">Password</label>
                                    <input type="password" ref={passwordRef} className="form-control" id="Password" />
                                </div>
                                <div className="d-flex justify-content-between">
                                    <button type="button" className="btn btn-success mr-5" data-bs-dismiss="modal">Cancel</button>
                                    <button type="submit" className="btn btn-danger" onClick={handleDeleteAccount}>Delete Account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Settings;