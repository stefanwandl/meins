import React, { useState, useEffect, useRef, useContext } from "react";
import { ListGroup, Form, Col, Button, Row } from "react-bootstrap";
import socketio from "socket.io-client";
import { NameContext } from "./components/NameContext";

function Chat({gameCode}) {
    const socket = socketio.connect("http://207.246.94.140:4001");

  const [messages, setMessages] = useState([]);
  const messageFeedRef = useRef();
  const { name } = useContext(NameContext);
  const messageRef = useRef("");

  useEffect(() => {

    socket.on("message", ({message, name, timestamp}) => {
      handleNewMessage(message, name, timestamp);
    });

    return () => {
      socket.off("message");
    }

  }, []);

  const handleNewMessage = (message, name, timestamp) => {

    setMessages((messages) => [...messages, { message, name, timestamp }]);

    window.scrollTo(0, document.body.scrollHeight); 

  }

  const handleSend = async (e) => {
    e.preventDefault();

    socket.emit("message", {
      message: messageRef.current.value,
      name,
      timestamp: new Date(),
      token: localStorage.getItem('token')
    });

    messageRef.current.value = "";
    messageRef.current.focus();
  };


  return (
      <div class="container" ref={messageFeedRef}>
        <ListGroup className="Chat" as="ul">
        {messages.map((message) => (
            <ListGroup.Item
            as="li"
            active={message.name === name ? true : false}
            className="d-flex justify-content-between align-items-start"
            key={message.name + message.message + message.timestamp}
            >
            <div className="me-auto">
                <strong className="d-block">
                {message.name === name.name ? `You (${name})` : message.name}
                </strong>
                <span>{message.message}</span>
            </div>
            <span className="badge rounded-pill" style={{color: "black"}}>
                {new Date(message.timestamp).toLocaleTimeString()}
            </span>
            </ListGroup.Item>
        ))}
        </ListGroup>
        <Form onSubmit={handleSend} className="Sender mb-2">
            <Row>
                <Col sm={9} className="mt-2">
                    <Form.Control type="text" ref={messageRef}></Form.Control>
                </Col>
                <Col sm={3} className="mt-2">
                    <Button variant="primary" type="submit" style={{width: "100%"}} block>
                        Send
                    </Button>
                </Col>
            </Row>
        </Form>
    </div>
  );
}

export default Chat;
