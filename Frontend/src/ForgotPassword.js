import React, { useState } from 'react';
import {
    Link
} from "react-router-dom";

function ForgotPassword() {
    const [email, setEmail] = useState("");
    const [error, setError] = useState("");
    const [gotEmail, setGotEmail] = useState(false);



    async function handlePasswordReset() {
        setError("");
        setGotEmail("");

        let _error = validateEmail();
        if (_error === "") {
            await fetch('http://207.246.94.140:4000/graphql', {
                method: 'POST',
                body: JSON.stringify(
                    {
                        query: `
                            query {
                                resetPw(email: "${email}") {
                                    userId:,
                                    expiration,
                                    token
                                }
                            }
                    `}
                ),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + window.localStorage.getItem("token")
                }
            }).then(res => {
                if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
                    throw new Error('Network error!');
                }
                return res.json();
            }).then(resData => {
                if (resData.errors === undefined) {
                    setGotEmail(true);
                    localStorage.setItem('token', resData.data.resetPw.token);
                } else {
                    _error = resData.errors[0].message;
                }
            }).catch(err => {
                console.log(err);
            });
        }
        setError(_error);
    }

    const validateEmail = () => {
        if (email === "" || !email.includes("@") || email.length >= 255) {
            return "Invalid email"
        }

        return "";
    }

    return (
        <div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
            <form className="ForgotPassword container custom-form" onSubmit={(e) => e.preventDefault()}>
                {
                    gotEmail &&
                    <div className="alert alert-success" role="alert">
                        An email with instructions to reset your password was sent to you. Please click on the link in it.
                    </div>
                }
                <div className="mb-3">
                    <label htmlFor="email" className="form-label" aria-describedby="emailHelp">Email address</label>
                    <input type="text" className={error !== "" ? "form-control is-invalid" : "form-control"} id="email" onChange={(e) => setEmail(e.target.value)} value={email} />
                    <div className={error !== "" ? "invalid-feedback" : "hidden"} >
                        {error}
                    </div>
                    <div id="emailHelp" className="form-text float-right">You will get an email with instruction to reset your password</div>
                </div>
                <button type="submit" className="btn btn-primary btn-block w-100" onClick={handlePasswordReset}>Request a new password</button>
                <div className="mt-2">Remembered your password? <Link to="/signin">Sign in here</Link></div>
            </form>
        </div>
    );
}

export default ForgotPassword;